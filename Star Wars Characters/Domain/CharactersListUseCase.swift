//
//  CharactersListUseCase.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation


class CharactersListUseCase {
    
    private var repository: CharactersListRepository
    private var apiCall: ApiCall
    
    init(repository: CharactersListRepository, apiCall: ApiCall) {
        self.repository = repository
        self.apiCall = apiCall
    }
    
    func fetchCharacters(completion: @escaping CharacterResponseCompletion) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.repository.fetchCharacters(completion: completion)
        }

    }
    
    func cancelFetchCharacters(repository: CharactersListRepository) {
        repository.cancelFetchCharacters()
    }
}
