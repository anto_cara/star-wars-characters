//
//  Character.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation


struct Character {
    var name: String = ""
    var height: String = ""
    var gender: String = ""
    var mass: String = ""
    
    init() {
        
    }
    
    init(name: String, height: String, gender: String, mass: String) {
        self.name = name
        self.height = height
        self.gender = gender
        self.mass = mass
    }
    
    static func parseFromJson(dictionaryData: Dictionary<String, AnyObject>) -> Character? {
        
        guard let name = dictionaryData["name"] as? String,
            let height = dictionaryData["height"] as? String,
            let gender = dictionaryData["gender"] as? String,
            let mass = dictionaryData["mass"] as? String else {
                return nil
        }
        return Character(name: name, height: height, gender: gender, mass: mass)
    }
}
