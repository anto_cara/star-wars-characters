//
//  CharactersListRepository.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation


class CharactersListRepository {
    
    private var apiCall: ApiCall?
    
    init(apicall: ApiCall) {
        self.apiCall = apicall
    }
    
    
    func fetchCharacters( completion: @escaping CharacterResponseCompletion) {
        apiCall?.fetchCharacters(completion: completion)
    }
    
    func cancelFetchCharacters(){
        apiCall?.cancelFetch()
    }
}
