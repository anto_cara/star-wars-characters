//
//  ApiCalls.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation


enum CharacterResponse<T>{
    case None
    case Success(T)
}

typealias CharacterResponseCompletion = (_ response: CharacterResponse<[Character]>?) -> (Void)

class ApiCall{
    
    private let GET = "GET"
    private var dataTask: URLSessionDataTask?
    
    func fetchCharacters(completion: @escaping CharacterResponseCompletion){
        
        guard let request = createRequest() else{
            return completion(.None)
        }
        
        let session = URLSession.shared
        dataTask = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            self.logResponse(response: response, error: error)
            
            guard let dictionaryResponse = self.responseToDictionary(data: data) else{
                return completion(.None)
            }
            print(dictionaryResponse)
            let characters = Parser.fromDictionaryToCharacters(dictionaryData: dictionaryResponse) as [Character]
            completion(CharacterResponse<[Character]>.Success(characters))
        })
        
        dataTask?.resume()
    }
    
    func cancelFetch() {
        dataTask?.cancel()
    }
    
    
    private func createRequest() -> URLRequest?{
        guard let url = URL(string: Constants.API_URL) else{
            return nil
        }
        var request = URLRequest(url: url)
        request.httpMethod = GET
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }
    
    private func responseToDictionary(data: Data?) -> Dictionary<String, AnyObject>?{
        do {
            guard let data = data, let dictionaryData = try JSONSerialization.jsonObject(with: data) as? Dictionary<String, AnyObject> else{
                return nil
            }
            return dictionaryData
            
        } catch {
            return nil
        }
    }
    
    private func logResponse(response: URLResponse?, error: Error?){
        if error == nil{
            print((response != nil) ? "\("Response Api Call ==> ", response)" : "Error in response")
        }
    }
}
