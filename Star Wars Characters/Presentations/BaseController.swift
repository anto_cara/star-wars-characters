//
//  BaseController.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation
import UIKit


class BaseController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    internal var spinner: UIActivityIndicatorView?
    internal var labelError: UILabel?
    
    
    internal func registerCellNib(cellNibName: String, identifierCell: String, tableView: UITableView) {
        let cellNib = UINib(nibName: cellNibName, bundle: nil);
        tableView.register(cellNib, forCellReuseIdentifier: identifierCell);
    }
    
    
    internal func configAutoHeightCells(tableView: UITableView){
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
    }
    
    
    internal func createTableViewFooter(){
        createLoadingInfineteScroll()
        createEmptyStateTableViewFooter()
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0,
                                              width: tableView.frame.width, height: 101))
        footerView.addSubview(spinner!)
        footerView.addSubview(labelError!)
        self.tableView.tableFooterView = footerView
    }
    
    internal func createLoadingInfineteScroll(){
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner?.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44)
    }
    
    internal func createEmptyStateTableViewFooter(){
        labelError = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 101))
        labelError?.textAlignment = NSTextAlignment.center
        labelError?.text = "Sorry, an error ocurried, try again"
        labelError?.isHidden = true
    }
}
