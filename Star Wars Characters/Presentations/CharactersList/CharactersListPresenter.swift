//
//  CharactersListPresenter.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation


class CharacterListPresenter {
    
    private var viewModel: CharacterListsViewModel
    private var repository: CharactersListRepository
    
    init(viewModel: CharacterListsViewModel, repository: CharactersListRepository) {
        self.viewModel = viewModel
        self.repository = repository
    }
    
    
    
    func fetchCharacters() {
        viewModel.displayLoadingData()
        repository.fetchCharacters { response in
            DispatchQueue.main.async {
                self.handleResponse(response: response)
            }
        }
    }
    
    func cancelFetch() {
        repository.cancelFetchCharacters()
        viewModel.hideLoadingData()
    }
    
    
    private func handleResponse(response: CharacterResponse<[Character]>?){
        guard let response = response else{
           return self.viewModel.displayEmptyState()
        }
        switch response {
        case .Success(let characters):
            self.viewModel.displayCharacters(characters: characters)
        case .None :
            self.viewModel.displayEmptyState()
        }
        self.viewModel.hideLoadingData()
    }
}
