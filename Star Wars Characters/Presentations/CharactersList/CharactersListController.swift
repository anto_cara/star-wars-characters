//
//  ViewController.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import UIKit

class CharactersListController: BaseController {
    
    
    private var characters =  [Character]()
    private var presenter: CharacterListPresenter?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeCells()
        initializePresenter()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        createTableViewFooter()
    }

    private func initializeCells(){
        self.registerCellNib(cellNibName: Constants.CHARACTER_CELL_XIB,
                             identifierCell: Constants.CHARACTER_CELL_IDENTIFIER,
                             tableView: self.tableView)
        self.configAutoHeightCells(tableView: tableView)
    }

    
    private func initializePresenter(){
        presenter = CharacterListPresenter(viewModel: self,
                                           repository: CharactersListRepository(apicall: ApiCall()))
    }
    
    private func displayCharacters(){
       presenter?.fetchCharacters()
    }
    
}

//MARK: ACTIONS
extension CharactersListController{
    @IBAction func cancelFetchEvent(){
        presenter?.cancelFetch()
    }
    
    @IBAction func fetchData(){
        presenter?.fetchCharacters()
    }
}

extension CharactersListController: CharacterListsViewModel{
    
    func displayCharacters(characters: [Character]){
        self.characters.append(contentsOf: characters)
        self.tableView.reloadData()
    }
    
    func displayEmptyState(){
        labelError?.isHidden = false
        spinner?.isHidden = true
    }
    
    func displayLoadingData(){
        labelError?.isHidden = true
        spinner?.startAnimating()
        spinner?.isHidden = false
       
    }
    
    func hideLoadingData(){
        spinner?.stopAnimating()
        spinner?.isHidden = true
    }
}

//MARK: -TABLE DELEGATES -
extension CharactersListController: UITableViewDataSource, UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CHARACTER_CELL_IDENTIFIER,  for: indexPath) as? CharacterListCell else{
            return UITableViewCell()
        }
        cell.bindData(character: characterAt(indexPath: indexPath))
        return cell
    }
    
    private func characterAt(indexPath: IndexPath) -> Character{
        return characters[indexPath.row]
    }
    
    
    func tableView(_ tableView: UITableView,  willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == characters.count - 1) {
            fetchData()
        }
    }
    

}

