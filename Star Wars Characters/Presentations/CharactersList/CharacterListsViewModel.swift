//
//  CharacterListsViewModel.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation



protocol CharacterListsViewModel{
    
    func displayCharacters(characters: [Character])
    func displayEmptyState()
    func displayLoadingData()
    func hideLoadingData()
}
