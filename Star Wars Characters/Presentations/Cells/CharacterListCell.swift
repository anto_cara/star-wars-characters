//
//  CharacterListCell.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation
import UIKit



class CharacterListCell: UITableViewCell{
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var mass: UILabel!
    @IBOutlet weak var gender: UILabel!
    
    
    func bindData(character: Character) {
        name.text = character.name
        height.text = character.height
        mass.text = character.mass
        gender.text = character.gender
    }
}
