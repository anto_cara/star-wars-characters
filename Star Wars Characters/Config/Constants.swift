//
//  Constants.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation



struct Constants {
    
    static let CHARACTER_CELL_XIB = "CharacterListCell"
    static let CHARACTER_CELL_VIEW = "CharacterListCell"
    static let CHARACTER_CELL_IDENTIFIER = "characterCellIdentifier"
    
    
    static let API_URL = "https://swapi.co/api/people/"

}
