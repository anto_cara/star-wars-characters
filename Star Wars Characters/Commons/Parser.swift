//
//  Parser.swift
//  Star Wars Characters
//
//  Created by Antonio Carabantes Millán on 28/6/18.
//  Copyright © 2018 Antonio Carabantes Millán. All rights reserved.
//

import Foundation

class Parser {
    
    
    class func fromDictionaryToCharacters(dictionaryData:  Dictionary<String, AnyObject>) -> [Character] {
        
        guard let results = (dictionaryData["results"] as? [AnyObject]) else{
            return [Character]()
        }
        
        return results.map {
            Character.parseFromJson(dictionaryData: $0 as! Dictionary<String, AnyObject>)!
        }
    
    }
}
